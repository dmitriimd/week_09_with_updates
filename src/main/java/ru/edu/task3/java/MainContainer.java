package ru.edu.task3.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class MainContainer {

    private DependencyObject dependency;


    public MainContainer(DependencyObject dependencyBean) {
        dependency = dependencyBean;
    }

    public String getValue() {
        return dependency.getValue();
    }
}
