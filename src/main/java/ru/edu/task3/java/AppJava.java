package ru.edu.task3.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
public class AppJava {

    public static MainContainer run(String profile){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles(profile);
        context.register(AppJava.class);
        context.refresh();
        return context.getBean(MainContainer.class);
    }
}
